<?php

/**
 * This is the model class for table "question".
 *
 * The followings are the available columns in table 'question':
 * @property integer $idQuestion
 * @property integer $fkIdQuestion
 * @property string $varcharDescription
 * @property string $varcharNameAnimal
 */
class Question extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Question the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'question';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('varcharDescription, varcharNameAnimal', 'required'),
			array('fkIdQuestion', 'numerical', 'integerOnly'=>true),
			array('varcharDescription, varcharNameAnimal', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idQuestion, fkIdQuestion, varcharDescription, varcharNameAnimal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idQuestion' => 'Id Question',
			'fkIdQuestion' => 'Fk Id Question',
			'varcharDescription' => 'Varchar Description',
			'varcharNameAnimal' => 'Varchar Name Animal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idQuestion',$this->idQuestion);
		$criteria->compare('fkIdQuestion',$this->fkIdQuestion);
		$criteria->compare('varcharDescription',$this->varcharDescription,true);
		$criteria->compare('varcharNameAnimal',$this->varcharNameAnimal,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}