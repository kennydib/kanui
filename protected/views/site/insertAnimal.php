<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'question-insertAnimal-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'varcharDescription'); ?>
		<?php echo $form->textField($model,'varcharDescription'); ?>
		<?php echo $form->error($model,'varcharDescription'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'varcharNameAnimal'); ?>
		<?php echo $form->textField($model,'varcharNameAnimal'); ?>
		<?php echo $form->error($model,'varcharNameAnimal'); ?>
	</div>

	<div class="row">
		<?php echo $form->hiddenField($model,'fkIdQuestion',array('value'=>$idQuestion)); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div>