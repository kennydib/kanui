<?php
class SiteController extends Controller
{
	public function actionIndex()
	{
		if($_POST['value'] == 'YES')
		{	
			$resultAnimal = $this->getQuestions($_POST['idQuestion'], NULL,1);
			if(!isset($resultAnimal['varcharNameAnimal']))
			{
				$this->redirect(array('site/insert/r='.$_POST['idQuestion']));	
			}
		}
		elseif($_POST['value'] == 'NO') 
		{
			if($_POST['fkIdQuestion'] == NULL)
			{
				$id = $_POST['idQuestion']+1;
				if($id < 3)
					$resultAnimal = $this->getQuestions($id);
				else
					$this->redirect(array('site/index'));
					
			}
			else
			{
				$resultAnimal = $this->getQuestions($_POST['idQuestion'], $_POST['fkIdQuestion'],2);
				if(!isset($resultAnimal['varcharNameAnimal']))
				{
					$resultAnimal = $this->getQuestions($_POST['fkIdQuestion'], $_POST['idQuestion'],3);
					if(!isset($resultAnimal['varcharNameAnimal']))
						$this->redirect(array('site/insert/r='.$_POST['fkIdQuestion']));		
				}		
					
			}
		}
		else 
		{
			$resultAnimal = $this->getQuestions();
		}
		
		$nameAnimal 			= $resultAnimal['varcharNameAnimal'];
		$questionDescription	= $resultAnimal['varcharDescription'];
		$idQuestion				= $resultAnimal['idQuestion'];
		$fkIdQuestion 			= $resultAnimal['fkIdQuestion'];
		
		$yes = ($nameAnimal == NULL ? 'answer' : 'showAnimal');
		
		$this->render('index',array(
									   'question'			=>	$questionDescription,
									   'idQuestion'			=>	$idQuestion,
									   'nameAnimal'			=>  $nameAnimal,
									   'fkIdQuestion'		=>	$fkIdQuestion,
									   'yes'				=>  $yes,
					 				)
					  );	  
	}
	
	public function actionInsert()
	{
		$model = new Question();

		if($_POST['value']=='NO')	
		{
			$idQuestion = $fkIdQuestion = $_POST['idQuestion'];
		}
		else
		{
			$string = explode("=", $_GET['r']);
			$idQuestion = $string['1'];
		}	
		
		if(isset($_POST['Question']))
		{
			$model->attributes = $_POST['Question'];
			if($model->save())
				$this->redirect(array('site/index'));
		}
		$this->render('insertAnimal',array('model'=>$model,'idQuestion'=>$idQuestion));
	}
	
	private function getQuestions($idQuestion = 0, $idFkQuestion = NULL,$typeQuery = NULL)
	{
		$model = new Question();

		if($typeQuery == NULL)
			return $question = $model->findByPk($idQuestion);
		elseif($typeQuery == 1)
			return $question = $model->find('fkIdQuestion = :fkIdQuestion', array(':fkIdQuestion'=>$idQuestion));
		elseif($typeQuery == 2)	
			return $question = $model->find('fkIdQuestion = :fkIdQuestion AND idQuestion > :idQuestion' , array(':fkIdQuestion'=>$idFkQuestion, ':idQuestion'=>$idQuestion));
		elseif($typeQuery == 3)	
			return $question = $model->find('fkIdQuestion = :fkIdQuestion AND idQuestion > :idQuestion' , array(':fkIdQuestion'=>$idFkQuestion, ':idQuestion'=>$idQuestion));
	}
}